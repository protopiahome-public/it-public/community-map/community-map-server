const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";
import sanitize from '../../utilities/sanitize';

const { query } = require('nact');

const resource = "place";

module.exports = {

    Mutation:{
        changeTeam: async (obj, args, context, info) => {

			const collectionItemActor  = context.children.get("teams");
			
			if (args.input.description) {
				args.input.description = sanitize(args.input.description);
			}
			
			if (!(context.user.roles.includes("admin") || context.user.roles.includes("moderator"))) {
				delete args.input.owner_id;
				delete args.input.members_ids;
			}
				
			if (args.input.owner_id) {
				args.input.owner_id = new ObjectId(args.input.owner_id);
			}
			if (args.input.members_ids) {
				for(var i in args.input.members_ids) {
					args.input.members_ids[i] = new ObjectId(args.input.members_ids[i]);
				}
			}

			if(args.id){
                return (await query(collectionItemActor,  {"type": "team", search:{_id: args.id}, input: args.input, user: context.user }, 20000))[0];
            }else {
				args.input.owner_id = new ObjectId(context.user._id);
                return (await query(collectionItemActor, {"type": "team", input: args.input, user: context.user}, 20000))[0];
            }

        },
		deleteTeam: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("teams");

 			return (await query(collectionItemActor,  {"type": "team", search:{_id: args.id}, delete: true, user: context.user }, 20000))

        },
		addMemberToTeam: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const teamItemActor  = context.children.get("teams");

            await query(collectionItemActor,  {"type": "team", search:{_id: args.team_id}, full_input: {$push: {members_ids: new ObjectId(args.member_id)}}}, 20000)
			
			return (await query(teamItemActor,  {search:{_id: args.team_id}}, 20000))[0];

        },
		removeMemberFromTeam: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const teamItemActor  = context.children.get("teams");

            await query(collectionItemActor,  {"type": "team", search:{_id: args.team_id}, full_input: {$pull: {members_ids: new ObjectId(args.member_id)}}}, 20000)
			
			return (await query(teamItemActor,  {search:{_id: args.team_id}}, 20000))[0];

        },
		addMeToTeam: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const teamItemActor  = context.children.get("teams");

            await query(collectionItemActor,  {"type": "team", search:{_id: args.team_id}, full_input: {$push: {members_ids: new ObjectId(context.user._id)}}}, 20000)
			
			return (await query(teamItemActor,  {search:{_id: args.team_id}}, 20000))[0];

        },
		removeMeFromTeam: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const teamItemActor  = context.children.get("teams");

            await query(collectionItemActor,  {"type": "team", search:{_id: args.team_id}, full_input: {$pull: {members_ids: new ObjectId(context.user._id)}}}, 20000)
			
			return (await query(teamItemActor,  {search:{_id: args.team_id}}, 20000))[0];

        },
    },

    Query: {

        getTeams: async (obj, args, context, info) => {

            const collectionActor = context.children.get("teams");

            var places = await query(collectionActor,  {"type": "team"}, 20000);
			
			return places;
        },
		
		getMyTeams: async (obj, args, context, info) => {

            const collectionActor = context.children.get("teams");

            var places = await query(collectionActor,  {"type": "team", "search": {"owner_id": new ObjectId(context.user._id)}}, 20000);
			
			return places;
        },
		
		getTeam: async (obj, args, context, info) => {

            const collectionActor = context.children.get("teams");

            var teams = (await query(collectionActor,  {"type": "team", "search": {"_id": args.id}}, 20000))[0];
			
			return teams;
        },
    },
	Team: {
		members : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			if (obj.members_ids) {
				obj.members = await query(collectionItemActor, {search: {_id: {$in: obj.members_ids}}}, 20000)
			}
			return obj.members;
		},
		owner : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			return (await query(collectionItemActor, {search: {_id: obj.owner_id}}, 20000))[0];
		},
	}
  
}