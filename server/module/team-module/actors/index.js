import teamCollection from "./teamCollection";

const { spawnStateless} = require('nact');

export default function(serverActor){
	spawnStateless(serverActor, teamCollection, "teams");
}
