import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

const {ForbiddenError} = require('apollo-server');

export default async function(msg, ctx)
{

    let teams = [];
    let match;

	if (!msg.search) {
		msg.search = {};
	}
	match = msg.search;
	if (match._id) {
		if (match._id.$in) {
			match._id.$in.map(s => new ObjectId(s));
		} else {
			match._id = new ObjectId(match._id);
		}
	}
	
	try {
	if (match._id && (msg.input || msg.full_input || msg.delete)) {
		var current_team = await db.team.findOne({_id: match._id, owner_id: new ObjectId(msg.user._id)});
		if (!current_team && !(msg.user.roles.includes('admin') || msg.user.roles.includes('moderator') && !msg.delete)) {
			throw new ForbiddenError('Resource Forbidden');
		}
	}
	if (msg.delete) {
		await db.team.remove(match);
		dispatch(ctx.sender,  true, ctx.self);
	}

	if (msg.input) {
		if (match._id) {
			await db.team.update(match, {$set: msg.input});
		} else {
			msg.input.owner_id = new ObjectId(msg.user._id);
			msg.input.members_ids = [new ObjectId(msg.user._id)];
			match = {
				_id: new ObjectId((await db.team.insert(msg.input))._id)
			}
		}
	}
	if (msg.full_input) {
		if (match._id) {
			await db.team.update(match, msg.full_input);
		} else {
			match = {
				_id: new ObjectId((await db.team.insert(msg.full_input))._id)
			}
		}
	}
	
    teams = await db.team.aggregate(
        [
            {"$match": match},
			{$sort: {"title" : 1}}
        ]);
		
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  teams, ctx.self);

}