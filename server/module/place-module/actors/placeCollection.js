import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

const {ForbiddenError} = require('apollo-server');

export default async function(msg, ctx)
{

    let places = [];
    let match;

	if (!msg.search) {
		msg.search = {};
	}
	match = msg.search;
	if (match._id) {
		if (match._id.$in) {
			match._id.$in.map(s => new ObjectId(s));
		} else {
			match._id = new ObjectId(match._id);
		}
	}
	
	try {
	if (match._id && (msg.input || msg.full_input || msg.delete)) {
		var current_place = await db.place.findOne({_id: match._id, owner_id: new ObjectId(msg.user._id)});
		if (!current_place && !(msg.user.roles.includes('admin') || msg.user.roles.includes('moderator') && !msg.delete)) {
			throw new ForbiddenError('Resource Forbidden');
		}
	}
	if (msg.delete) {
		await db.place.remove(match);
		dispatch(ctx.sender,  true, ctx.self);
	}
	if (msg.input) {
		if (match._id) {
			await db.place.update(match, {$set: msg.input});
		} else {
			msg.input.owner_id = new ObjectId(msg.user._id);
			msg.input.members_ids = [new ObjectId(msg.user._id)];
			match = {
				_id: new ObjectId((await db.place.insert(msg.input))._id)
			}
		}
	}
	if (msg.full_input) {
		if (match._id) {
			await db.place.update(match, msg.full_input);
		} else {
			match = {
				_id: new ObjectId((await db.place.insert(msg.full_input))._id)
			}
		}
	}
    places = await db.place.aggregate(
        [
            {"$match": match},
			{
				"$lookup": {
					"from":         "place_event",
					"localField":   "events_ids",
					"foreignField": "_id",
					"as":           "events"
				}
			},
			{$sort: {"title" : 1}}
        ]);
		
		places.forEach((p)=>{
			p.geo = [p.latitude, p.longitude]

			p.is_events_active = false;
			let daysAgo = new Date();
			daysAgo.setDate(daysAgo.getDate() - 10);
			let daysForward = new Date();
			daysForward.setDate(daysForward.getDate() + 30);
			p.events.forEach((e)=>{
				if (new Date(e.start_date) > daysAgo && new Date(e.start_date) < daysForward) {
					p.is_events_active = true;
				}
			});
		});
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  places, ctx.self);

}