const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";
const axios = require("axios");
import sanitize from '../../utilities/sanitize';
import config from '../../config/config';

const { query } = require('nact');

const resource = "place";

module.exports = {

    Mutation:{
        changePlace: async (obj, args, context, info) => {

			const collectionItemActor  = context.children.get("places");
			
			if (args.input.description) {
				args.input.description = sanitize(args.input.description);
			}
			if (args.input.information) {
				args.input.information = sanitize(args.input.information);
			}
			if (args.input.projects) {
				args.input.projects = sanitize(args.input.projects);
			}
			if (args.input.members_aux) {
				args.input.members_aux = sanitize(args.input.members_aux);
			}

			if (!(context.user.roles.includes("admin") || context.user.roles.includes("moderator"))) {
				delete args.input.owner_id;
				delete args.input.members_ids;
				delete args.input.teams_ids;
			}
				
			if (args.input.owner_id) {
				args.input.owner_id = new ObjectId(args.input.owner_id);
			}
			if (args.input.members_ids) {
				for(var i in args.input.members_ids) {
					args.input.members_ids[i] = new ObjectId(args.input.members_ids[i]);
				}
			}
			if (args.input.teams_ids) {
				for(var i in args.input.teams_ids) {
					args.input.teams_ids[i] = new ObjectId(args.input.teams_ids[i]);
				}
			}

 			if (args.input.geo) {
				args.input.latitude = args.input.geo[0];
				args.input.longitude = args.input.geo[1];

				let geocode = await axios.get("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + args.input.longitude + "," + args.input.latitude + "&format=json");
				geocode = geocode.data;
				//console.log(JSON.stringify(geocode, null, 4));
				//console.log(geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components)
				let components = geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
				let geo_analytics = {};
				components.forEach((component) => {
					let key = component.kind;
					let value = component.name;
					if (geo_analytics[key]) {
						key = key + "2";
					}
					geo_analytics[key] = value;
				});
				args.input.geo_analytics = geo_analytics;

				delete args.geo;
			}

			if(args.id){
                return (await query(collectionItemActor,  {"type": "place", search:{_id: args.id}, input: args.input, user: context.user }, 20000))[0];
            }else {
				args.input.owner_id = new ObjectId(context.user._id);
                return (await query(collectionItemActor, {"type": "place", input: args.input, user: context.user}, 20000))[0];
            }

        },
		deletePlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("places");

 			return (await query(collectionItemActor,  {"type": "place", search:{_id: args.id}, delete: true, user: context.user }, 20000))

        },
		changePlaceType: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");

            if(args.id){
				return await query(collectionItemActor,  {"type": "place_type", search:{_id: args.id}, input: args.input }, 20000);
            }else {
                return await query(collectionItemActor, {"type": "place_type", input: args.input}, 20000);
            }

        },
		deletePlaceType: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");

 			return await context.db.place_type.remove({_id: new ObjectId(args.id)})

        },
		changePlaceEvent: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");

            if(args.id){
				if (!(context.user.roles.includes('admin') || context.user.roles.includes('moderator'))) {
					let place = await query(collectionItemActor,  {"type": "place", search: {events_ids: {$in:[new ObjectId(args.id)]}, owner_id: new ObjectId(context.user._id)}, user: context.user}, 20000);
					if (!place) {
						return;
					}
				}
				return await query(collectionItemActor,  {"type": "place_event", search:{_id: args.id}, input: args.input }, 20000);
            }else {
                return await query(collectionItemActor, {"type": "place_event", input: args.input}, 20000);
            }

        },
		deletePlaceEvent: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("places");

			let current_place = (await query(collectionItemActor,  {"type": "place", search:{_id: args.id, owner_id: new ObjectId(context.user._id)}, delete: true, user: context.user }, 20000));
			if (!current_place && !msg.user.roles.contains('admin')) {
				throw new ForbiddenError('Resource Forbidden');
			}
			await context.db.place_event.remove({_id: new ObjectId(args.id)});
			return true; 

        },
		addEventToPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            return (await query(collectionItemActor,  {type: "place", search:{_id: args.place_id}, full_input: {$push: {events_ids: new ObjectId(args.event_id)}}, user: context.user }, 20000));

        },
		addMemberToPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            await query(collectionItemActor,  {"type": "place", search:{_id: args.place_id}, full_input: {$push: {members_ids: new ObjectId(args.member_id)}}}, 20000);
			
			return (await query(placeItemActor,  {search:{_id: args.place_id}}, 20000))[0];

        },
		removeMemberFromPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            await query(collectionItemActor,  {"type": "place", search:{_id: args.place_id}, full_input: {$pull: {members_ids: new ObjectId(args.member_id)}}}, 20000)
			
			return (await query(placeItemActor,  {search:{_id: args.place_id}}, 20000))[0];

        },
		addMeToPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            await query(collectionItemActor,  {"type": "place", search:{_id: args.place_id}, full_input: {$push: {members_ids: new ObjectId(context.user._id)}}}, 20000)
			
			return (await query(placeItemActor,  {search:{_id: args.place_id}}, 20000))[0];

        },
		removeMeFromPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            await query(collectionItemActor,  {"type": "place", search:{_id: args.place_id}, full_input: {$pull: {members_ids: new ObjectId(context.user._id)}}}, 20000)
			
			return (await query(placeItemActor,  {search:{_id: args.place_id}}, 20000))[0];

        },
		mergePlaces: async (obj, args, context, info) => {
			if (args.from_id == args.to_id) {
				return;
			}
			
			let from_place = await context.db.place.findOne({_id: new ObjectId(args.from_id)});
			let to_place = await context.db.place.findOne({_id: new ObjectId(args.to_id)});
			if (!from_place || !to_place) {
				return;
			}
			
			let merge_array = function(field_name) {
				if (!from_place[field_name]) {
					from_place[field_name] = [];
				}
				if (!to_place[field_name]) {
					to_place[field_name] = [];
				}
				from_place[field_name].forEach((p)=>{
					to_place[field_name].push(p);
				});
			}
			merge_array("events_ids");
			merge_array("teams_ids");
			merge_array("members_ids");
			to_place.title = from_place.title;
			to_place.description = from_place.description;
			to_place.information = from_place.information;
			to_place.type = from_place.type;
			to_place.type_new_id = new ObjectId(from_place.type_new_id);
			delete to_place._id;
			await context.db.place.update({_id: new ObjectId(args.to_id)}, to_place);
			await context.db.place.remove({_id: new ObjectId(args.from_id)});
			
            const collectionActor = context.children.get("places");

            var places = (await query(collectionActor,  {"type": "place", "search": {"_id": args.to_id}}, 20000))[0];
			
			return places;

		}
    },

    Query: {

        getMyPlaces: async (obj, args, context, info) => {


            const collectionActor = context.children.get("places");

            var places = await query(collectionActor,  {"type": "place", search: {owner_id: new ObjectId(context.user._id)}}, 20000);
			
			return places;


        },

        getPlaces: async (obj, args, context, info) => {

            const collectionActor = context.children.get("places");

            var places = await query(collectionActor,  {"type": "place"}, 20000);
			
			function distance(lat1, lon1, lat2, lon2, unit) {
				if ((lat1 == lat2) && (lon1 == lon2)) {
					return 0;
				}
				else {
					var radlat1 = Math.PI * lat1/180;
					var radlat2 = Math.PI * lat2/180;
					var theta = lon1-lon2;
					var radtheta = Math.PI * theta/180;
					var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
					if (dist > 1) {
						dist = 1;
					}
					dist = Math.acos(dist);
					dist = dist * 180/Math.PI;
					dist = dist * 60 * 1.1515;
					if (unit=="K") { dist = dist * 1.609344 }
					if (unit=="N") { dist = dist * 0.8684 }
					return dist;
				}
			}
			if (args.near_of) {
				var near_place = (await query(collectionActor,  {"type": "place", "search": {"_id": args.near_of}}, 20000))[0];
				places.sort((p1, p2)=>{
					return distance(p1.latitude, p1.longitude, near_place.latitude, near_place.longitude, "K") - distance(p2.latitude, p2.longitude, near_place.latitude, near_place.longitude, "K");
				});
			}
			
			return places;
        },
		
		getPlaceEvent: async (obj, args, context, info) => {

            const collectionActor = context.children.get("item");

            var places = await query(collectionActor,  {"type": "place_event", "search": {"_id": args.id}}, 20000);
			
			return places;
        },
		
		getPlaceEvents: async (obj, args, context, info) => {

            const collectionActor = context.children.get("collection");

            var places = await query(collectionActor,  {"type": "place_event"}, 20000);
			places.sort(function(a,b){return new Date(b.start_date) - new Date(a.start_date)});
			
			return places;
        },
		
		getPlace: async (obj, args, context, info) => {

            const collectionActor = context.children.get("places");

            var places = (await query(collectionActor,  {"type": "place", "search": {"_id": args.id}}, 20000))[0];
			
			return places;
        },
		
		getPlaceType: async (obj, args, context, info) => {

            const collectionActor = context.children.get("item");

            var places = await query(collectionActor,  {"type": "place_type", "search": {"_id": args.id}}, 20000);
			
			return places;
        },
		
		getPlaceTypes: async (obj, args, context, info) => {

            const collectionActor = context.children.get("collection");

            var places = await query(collectionActor,  {"type": "place_type"}, 20000);
			places.sort(function(a,b){return new Date(b.start_date) - new Date(a.start_date)});
			
			return places;
        },
		
    },
	Place: {
		members : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			if (obj.members_ids) {
				obj.members = await query(collectionItemActor, {search: {_id: {$in: obj.members_ids}}}, 20000)
			}
			return obj.members;
		},
		owner : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			return (await query(collectionItemActor, {search: {_id: obj.owner_id}}, 20000))[0];
		},
		type_new : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("item");
			return await query(collectionItemActor, {type: "place_type", search: {_id: obj.type_new_id}}, 20000);
		},
	},
	PlaceEvent: {
		place : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("places");
			obj.place = (await query(collectionItemActor, {search: {events_ids: new ObjectId(obj._id)}}, 20000))[0];
			return obj.place;
		},
	},
  
}