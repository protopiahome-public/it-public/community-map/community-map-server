// https://www.apollographql.com/docs/apollo-server/features/errors/

const {ApolloError} = require('apollo-server');

export class InvalidEmailError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_EMAIL');

        Object.defineProperty(this, 'name', {value: 'InvalidEmailError'});
    }
}
export class DoubleEmailError extends ApolloError {
    constructor(message: string) {
        super(message, 'DOUBLE_EMAIL');

        Object.defineProperty(this, 'name', {value: 'DoubleEmailError'});
    }
}
export class InvalidPasswordError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_PASSWORD');

        Object.defineProperty(this, 'name', {value: 'InvalidPasswordError'});
    }
}