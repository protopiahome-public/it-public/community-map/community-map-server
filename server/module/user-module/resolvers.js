import {DateTimeResolver} from 'graphql-scalars';
import {InvalidEmailError, DoubleEmailError, InvalidPasswordError} from './errors';

import bcrypt from "bcrypt";

const { query } = require('nact');

import {ObjectId} from "promised-mongo";

const axios = require("axios");
import config from '../../config/config';

const resource = "user";

module.exports = {

    DateTime: DateTimeResolver,
    Query: {

        userInfo: async (obj, args, ctx, info) => {

            return ctx.user;

        },

        getCurrentUser: async (obj, args, ctx, info) => {

            let user = await ctx.user;



            // const {collectionItemActor} = require("../../actors/actorSystem");

            const collectionItemActor = ctx.children.get("users");
            return (await query(collectionItemActor, {"type": "user", search: {_id: user._id}}, 20000))[0];

        },

        getUser: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../actors/actorSystem");

            const collectionItemActor = ctx.children.get("users");
            return (await query(collectionItemActor, {search: {_id: args._id}}, 20000))[0];

        },

        getUsers: async (obj, args, ctx, info) => {

            // const {collectionActor} = require("../../actors/actorSystem");


            const collectionActor = ctx.children.get("users");

            return await query(collectionActor, {}, 20000);

        },


        getUserByExternalId: async (obj, args, ctx, info) => {

            // const {collectionItemActor}  = require("../../actors/actorSystem");
            const collectionItemActor = ctx.children.get("users");

            args.external_id = parseInt(args.external_id);

            let user;
            switch (args.external_system) {
                case "telegram":
                    user = await query(collectionItemActor, {search:
                            {telegram_id: args.external_id}
                    }, 20000)
                    break;
                case "slack":
                    user = await query(collectionItemActor, {search:
                            {slack_id: args.external_id}
                    }, 20000);
                    break;
                case "vk":
                    user = await query(collectionItemActor, {search:
                            {vk_id: args.external_id}
                    }, 20000);
                    break;

            }

            return user[0];

        },

    },

    Mutation:{

		deleteUser: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");

 			return await context.db.user.remove({_id: new ObjectId(args.id)})

        },
        registerUser: async (obj, args, ctx, info) => {

            // const { collectionItemActor} = require("../../actors/actorSystem");
            const collectionItemActor = ctx.children.get("item");
			
			delete args.input.roles;

            if(args.input.password){
                const crypto_password = bcrypt.hashSync(args.input.password, 10);
                args.input.crypto_password = crypto_password;
                delete (args.input.password);
            } else {
				throw new InvalidPasswordError("Empty password");
			}

			if (!args.input.email.match(/^.+@.+$/)) {
				throw new InvalidEmailError("Wrong email");
			}
			let user = await query(collectionItemActor, {type: "user", search: {email: args.input.email}}, 20000);
			console.log(user)
			
			if (user) {
				throw new DoubleEmailError("Email already used");
            }
            
            if (args.input.address) {
                let geocode = await axios.get("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(args.input.address) + "&format=json");
                geocode = geocode.data;
                //console.log ("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(user.address) + "&format=json");
                //console.log(JSON.stringify(geocode, null, 4));
                //console.log(geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components)
                let components = geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
                let geo_analytics = {};
                components.forEach((component) => {
                    let key = component.kind;
                    let value = component.name;
                    if (geo_analytics[key]) {
                        key = key + "2";
                    }
                    geo_analytics[key] = value;
                });
                args.input.geo_analytics = geo_analytics;
            }

            args.input.roles = ['user'];

            return await query(collectionItemActor, {type: "user", input: args.input}, 20000);

        },

        changeUser: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");
			
			if(args.input.password){
                const crypto_password = bcrypt.hashSync(args.input.password, 10);
                args.input.crypto_password = crypto_password;
                delete (args.input.password);
            }

			if (!args.input.email || !args.input.email.match(/^.+\@.+$/)) {
				throw new InvalidEmailError("Wrong email");
			}
			let user = await query(collectionItemActor, {type: "user", search: {_id: {$ne: new ObjectId(args.id)}, email: args.input.email}}, 20000);
			console.log(user)
			
			if (user) {
				throw new DoubleEmailError("Email already used");
            }
            
            if (args.input.address) {
                let geocode = await axios.get("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(args.input.address) + "&format=json");
                geocode = geocode.data;
                //console.log ("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(user.address) + "&format=json");
                //console.log(JSON.stringify(geocode, null, 4));
                //console.log(geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components)
                let components = geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
                let geo_analytics = {};
                components.forEach((component) => {
                    let key = component.kind;
                    let value = component.name;
                    if (geo_analytics[key]) {
                        key = key + "2";
                    }
                    geo_analytics[key] = value;
                });
                args.input.geo_analytics = geo_analytics;
            }

            return await query(collectionItemActor,  {type: "user", search:{_id: args.id}, input: args.input }, 20000);


        },

        changeCurrentUser: async (obj, args, ctx, info) => {

            let client = await ctx.client;
            let current_user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");

            switch (client.application_type) {
                case "leaderid":
                    // args.input
                    break;
                case "telegram":
                    //args.input
                    break;
                case "vk":
                    //args.input
                    break;
                case "slack":
                    //args.input
                    break;
                case "email":
                    //args.input
                    break;
                default:
                    break;
            }

            if (args.input.address) {
                let geocode = await axios.get("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(args.input.address) + "&format=json");
                geocode = geocode.data;
                //console.log ("https://geocode-maps.yandex.ru/1.x/?apikey=" + config.yandex_key + "&geocode=" + encodeURIComponent(user.address) + "&format=json");
                //console.log(JSON.stringify(geocode, null, 4));
                //console.log(geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components)
                let components = geocode.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components;
                let geo_analytics = {};
                components.forEach((component) => {
                    let key = component.kind;
                    let value = component.name;
                    if (geo_analytics[key]) {
                        key = key + "2";
                    }
                    geo_analytics[key] = value;
                });
                args.input.geo_analytics = geo_analytics;
            }


            if (args.input.password) {
                let crypto_password = bcrypt.hashSync(args.input.password, 10);
                delete args.input.password;
                args.input.crypto_password = crypto_password;
                return await query(collectionItemActor, {type: "user", search:{_id: current_user._id}, input: args.input }, 20000);
            }else{
                return await query(collectionItemActor, {type: "user", search:{_id: current_user._id}, input: args.input }, 20000);
            }



        },

        changeUserRoles: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor}  = require("../../actors/actorSystem");

            return await query(collectionItemActor, {type: "user", search: {_id: args._id}, input: {roles: args.roles }  }, 20000);

        },

        connect: async (obj, args, ctx, info) => {

            let current_user = await ctx.user;

            const collectionItemActor = ctx.children.get("item");
            // const { collectionItemActor} = require("../../actors/actorSystem");

            let authenticator =  await query(collectionItemActor, {type: "authenticate_session", search: {"user_code": args.user_code} }, 20000);

            let user;
            switch (authenticator.oob_channel) {
                case "slack":
                    user = await query(collectionItemActor, {type: "user", search: {"slack_id": authenticator.slack_id}  }, 20000);
                    break;
                case "telegram":
                    user = await query(collectionItemActor, {type: "user", search: {"telegram_id": authenticator.telegram_id}  }, 20000);
                    break;
                case "vk":
                    user = await query(collectionItemActor, {type: "user", search: {"vk_id": authenticator.vk_id}  }, 20000);
                    break;
            }

            current_user = await query(collectionItemActor, {type: "user", search: {_id: current_user._id}, input:{
                    user_ids:[user._id],
                }}, 20000);

            user = await query(collectionItemActor, {type: "user", search: {"_id": user._id} , input:{is_remove: true} }, 20000);

            return user;

        },

    }


}