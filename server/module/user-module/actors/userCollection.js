import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx)
{

	try {
    let users = [];
    let match;

	if (!msg.search) {
		msg.search = {};
	}
	match = msg.search;
	if (match._id) {
		if (match._id.$in) {
			match._id.$in.map(s => new ObjectId(s));
		} else {
			match._id = new ObjectId(match._id);
		}
	}

    users = await db.user.aggregate(
        [
            {"$match": match},
			{"$sort" : {"name" : 1}}
        ]);
		
		users.forEach((e)=>{
			e.title = e.name
		});
    dispatch(ctx.sender,  users, ctx.self);
	} catch (e) {console.log(e);}

}