const { query } = require('nact');

module.exports = {

    Mutation: {
        register: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../../actorSystem");
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {"type": "client", input: args.input}, 20000);

        },

        updateClient: async (obj, args, ctx, info) => {

            // const {collectionItemActor} = require("../../../actorSystem");
            const collectionItemActor = ctx.children.get("item");

            return await query(collectionItemActor, {
                "type": "client",
                search: {_id: args._id},
                input: args.input
            }, 20000);

        },

        removeClient: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor} = require("../../../actorSystem");

            return await query(collectionItemActor, {
                "type": "client",
                search: {_id: args._id},
                input: {is_remove: true}
            }, 20000);

        },

    },
    Query:{
        getClients: async (obj, args, ctx, info) => {


            // const {collectionActor}  = require("../../../actorSystem");
            const collectionActor = ctx.children.get("collection");



            return await query(collectionActor,  {"type": "client"}, 20000);

        },

        getClient: async (obj, args, ctx, info) => {

            const collectionItemActor = ctx.children.get("item");
            // const {collectionItemActor}  = require("../../../actorSystem");

            return await query(collectionItemActor,  {"type": "client", _id: args._id}, 20000);

        },


    }
}