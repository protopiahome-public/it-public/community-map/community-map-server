import {GraphQLModule} from "@graphql-modules/core";

import resolvers from './resolvers';
import {readFileSync} from "fs";
import gql from "graphql-tag";
import db from "../../db";
import MainModule from "../main-module";
const schema = readFileSync(__dirname + '/schema.graphqls', 'utf8');

export default function(ctx) {

    const typeDefs = gql`
        ${schema}
    `;

    return  new GraphQLModule({
        name: "ClientsModule",
        resolvers: resolvers,
        typeDefs: typeDefs,
        imports: [MainModule(ctx)]
    });

}