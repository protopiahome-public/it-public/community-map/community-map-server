import AccessTokenType from "../../../../token-types/AccessTokenType";
import RefreshTokenType from "../../../../token-types/RefreshTokenType";
import IdTokenType from "../../../../token-types/IdTokenType";

export default async function(args, ctx) {

    const token = args.token;
    const token_type_hint = args.token_type_hint;

    switch (token_type_hint) {
        case "refresh_token":
            return new RefreshTokenType(token);
            break;
        case "access_token":
            return new AccessTokenType(token);
            break;
        case "id_token":
            return new IdTokenType(token);
            break;
        default:
            break;
    }

}
