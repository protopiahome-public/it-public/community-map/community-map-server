const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "place";

module.exports = {

    Mutation:{
		setChatToPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("places");
			const placeItemActor  = context.children.get("places");

			return (await query(collectionItemActor,  {search:{_id: args.place_id}, input: {chat_id: new ObjectId(args.chat_id)}, user: context.user }, 20000))[0];

        },
		addTeamToPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            await query(collectionItemActor,  {"type": "place", search:{_id: args.place_id}, full_input: {$push: {teams_ids: new ObjectId(args.team_id)}}, user: context.user }, 20000)
			
			return (await query(placeItemActor,  {search:{_id: args.place_id}}, 20000))[0];

        },
		removeTeamFromPlace: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			const placeItemActor  = context.children.get("places");

            await query(collectionItemActor,  {"type": "place", search:{_id: args.place_id}, full_input: {$pull: {teams_ids: new ObjectId(args.team_id)}}, user: context.user }, 20000)
			
			return (await query(placeItemActor,  {search:{_id: args.place_id}}, 20000))[0];

        },
		setModerator: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");
			
			let input = args.enabled ? {$push: {roles: "moderator"}} : {$pull: {roles: "moderator"}}

            return await query(collectionItemActor,  {"type": "user", search:{_id: args.user_id}, full_input: input, user: context.user }, 20000)

        },
    },

    Query: {

    },
	Place: {
		chat : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("chats");
			return (await query(collectionItemActor, {search: {_id: obj.chat_id}}, 20000))[0];
		},
		teams : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("teams");
			if (obj.teams_ids) {
				obj.teams = await query(collectionItemActor, {search: {_id: {$in: obj.teams_ids}}}, 20000)
			}
			return obj.teams;
		},
	}
  
}