import {GraphQLModule} from "@graphql-modules/core";

import resolvers from './resolvers';
import {readFileSync} from "fs";
import gql from "graphql-tag";
import MainModule from "../main-module";
import TeamModule from "../team-module";
import ChatModule from "../chat-module";
import PlaceModule from "../place-module";

export default function(ctx) {

    const schema = readFileSync(__dirname + '/schema.graphqls', 'utf8');


    const typeDefs = gql`
        ${schema}
    `;

    return new GraphQLModule({
        name: "LigaModule",
        resolvers: resolvers,
        typeDefs: schema,
        imports: [MainModule(ctx), TeamModule(ctx), ChatModule(ctx), PlaceModule(ctx)]

    });
}