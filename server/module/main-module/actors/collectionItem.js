import db from "../../../db";
import {ObjectId} from "promised-mongo";
const { dispatch, spawnStateless } = require('nact');

//Stateless - Сохраняющий свое состояние во внешний источник.
//https://www.npmjs.com/package/flux-standard-action
//https://nact.io/en_uk/lesson/javascript/hierarchy


export default (parent) => spawnStateless(
    parent,
    /**
     * Actor get one item
     *
     * @param msg.type collection type
     * @param msg.search search object
     * @param msg.input input object
     *
     * @return item item of collection
     */
    async (msg, ctx) =>
    {
        try {
            const type = msg.type.slice();
            // msg.search
            // _id: new ObjectId(msg._id)
            delete (msg.type);


            let item = {};

            if(msg.search && msg.input){
				
				if(msg.search._id){
                    msg.search._id = new ObjectId(msg.search._id);
                }

                item = await db.collection(type).findAndModify({
                    query: msg.search,
                    update: {
                        $set : msg.input
                    },
					new: true
                });

            }
            else if(msg.search && msg.full_input){

                if(msg.search._id){
                    msg.search._id = new ObjectId(msg.search._id);
                }

                item = await db.collection(type).findAndModify({
                    query: msg.search,
                    update: msg.full_input,
					new: true
                });

            }
            else if(msg.input){
                item = await db.collection(type).insert(msg.input);
            }else if (msg.search){
                if(msg.search._id && typeof msg.search._id === "string"){
                    msg.search._id = new ObjectId(msg.search._id);
                }

                item = await db.collection(type).findOne(msg.search);

            }

            dispatch(ctx.sender,   item , ctx.self);
        } catch (e) {
            console.log(e);
        }
    },
    "item"
);