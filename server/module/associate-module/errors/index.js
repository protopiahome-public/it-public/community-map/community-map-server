// https://www.apollographql.com/docs/apollo-server/features/errors/

const {ApolloError} = require('apollo-server');

export class InvalidAuthenticatorTypeError extends ApolloError {
    constructor(message: string) {
        super(message, 'INVALID_AUTHENTICATOR_TYPE');

        Object.defineProperty(this, 'name', {value: 'InvalidAuthenticatorTypeError'});
    }
}