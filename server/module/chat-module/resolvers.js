const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "place";

module.exports = {

    Mutation:{
		changeChat: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("item");

            if(args.id){
                //return await query(collectionItemActor,  {"type": "chat", search:{_id: args.id}, input: args.input }, 20000);
            }else {
				var current_chat = await query(collectionItemActor, {"type": "chat", search: {external_id: args.input.external_id, external_system: args.input.external_system}}, 20000);
				if (current_chat) {
					return current_chat;
				}
                return await query(collectionItemActor, {"type": "chat", input: args.input}, 20000);
            }

        },
    },

    Query: {

	},
	Chat: {
		admins : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			if (obj.admins_ids) {
				obj.admins = await query(collectionItemActor, {search: {_id: {$in: obj.admins_ids}}}, 20000)
			}
			return obj.admins;
		},
	}
  
}