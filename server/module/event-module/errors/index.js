// https://www.apollographql.com/docs/apollo-server/features/errors/

const {ApolloError} = require('apollo-server');

export class EventNotFoundError extends ApolloError {
    constructor(message: string) {
        super(message, 'EVENT_NOT_FOUND');

        Object.defineProperty(this, 'name', {value: 'EventNotFoundError'});
    }
}