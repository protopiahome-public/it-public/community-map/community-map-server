import db from "../../../db";

import {ObjectId} from "promised-mongo";

const { dispatch } = require('nact');

export default async function(msg, ctx)
{

    let events = [];
    let match;

	if (!msg.search) {
		msg.search = {};
	}
	match = msg.search;
	if (match._id) {
		if (match._id.$in) {
			match._id.$in.map(s => new ObjectId(s));
		} else {
			match._id = new ObjectId(match._id);
		}
	}
	
	try {
	if (msg.input) {
		if (match._id) {
			await db.event.update(match, {$set: msg.input});
		} else {
			match = {
				_id: new ObjectId((await db.event.insert(msg.input))._id)
			}
		}
	}
    events = await db.event.aggregate(
        [
            {"$match": match},
			{$sort: {"title" : 1}}
        ]);
		events.forEach((e)=>{
			e.geo = [e.latitude, e.longitude]
		});
	} catch (e) {console.log(e);}
    dispatch(ctx.sender,  events, ctx.self);

}