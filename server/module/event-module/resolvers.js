const {AuthenticationError, ForbiddenError} = require('apollo-server');
// import {AuthenticationError} from 'apollo-server';
import {ObjectId} from "promised-mongo";

const { query } = require('nact');

const resource = "evemt";

module.exports = {

    Mutation:{
		changeEvent: async (obj, args, context, info) => {

            const collectionItemActor  = context.children.get("events");
 			if (args.input.geo) {
				args.input.latitude = args.input.geo[0];
				args.input.longitude = args.input.geo[1];
				delete args.geo;
			}

			if(args.id){
                return (await query(collectionItemActor,  {"type": "event", search:{_id: args.id}, input: args.input }, 20000))[0];
            }else {
                return (await query(collectionItemActor, {"type": "event", input: args.input}, 20000))[0];
            }

        },
		signUpEvent: async (obj, args, context, info) => {
			
			const collectionItemActor  = context.children.get("item");
			
			return await query(collectionItemActor,  {"type": "event", search:{_id: args.event_id}, full_input: {$push: {signup_members_ids: new ObjectId(context.user._id)}} }, 20000);

        },
		checkInEvent: async (obj, args, context, info) => {
			function distance(lat1, lon1, lat2, lon2) {
				var unit = "K";
				if ((lat1 == lat2) && (lon1 == lon2)) {
					return 0;
				}
				else {
					var radlat1 = Math.PI * lat1/180;
					var radlat2 = Math.PI * lat2/180;
					var theta = lon1-lon2;
					var radtheta = Math.PI * theta/180;
					var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
					if (dist > 1) {
						dist = 1;
					}
					dist = Math.acos(dist);
					dist = dist * 180/Math.PI;
					dist = dist * 60 * 1.1515;
					if (unit=="K") { dist = dist * 1.609344 }
					if (unit=="N") { dist = dist * 0.8684 }
					return dist;
				}
			}

            const collectionItemActor  = context.children.get("item");
			const collectionActor  = context.children.get("collection");
			var events = await query(collectionActor, {"type": "event"}, 20000);
			
			for (var event of events) {
				var dest = distance(event.latitude, event.longitude, args.check_in.latitude, args.check_in.longitude);
				var d_now = new Date();
				var d_event = new Date(event.start_date);
				d_now.setHours(0, 0, 0, 0);
				d_event.setHours(0, 0, 0, 0);
				if (dest < 1 && d_now.toDateString() == d_event.toDateString()/* && event.signup_members_ids.includes(context.user._id)*/) {
					return await query(collectionItemActor,  {"type": "event", search:{_id: event._id}, full_input: {$push: {members_ids: new ObjectId(context.user._id)}} }, 20000);
				}
           }

        },
    },

    Query: {

		getMyEvents: async (obj, args, context, info) => {

            const collectionActor = context.children.get("events");

            var events = await query(collectionActor,  {"type": "event", search: {members_ids: context.user._id}}, 20000);
			
			return events;
        },
		
		
		
		getEventByExternal: async (obj, args, context, info) => {

            const collectionActor = context.children.get("events");

            var places = (await query(collectionActor,  {"type": "event", "search": args}, 20000))[0];
			
			return places;
        },
		
		getEvent: async (obj, args, context, info) => {

            const collectionActor = context.children.get("events");

            var places = (await query(collectionActor,  {"type": "event", "search": {"_id": args.id}}, 20000))[0];
			
			return places;
        },

		getEvents: async (obj, args, context, info) => {

            const collectionActor = context.children.get("events");

            var events = await query(collectionActor,  {"type": "event"}, 20000);
			return events;
        },
    },
	Event: {
		members : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			if (obj.members_ids) {
				obj.members = await query(collectionItemActor, {search: {_id: {$in: obj.members_ids}}}, 20000)
			}
			return obj.members;
		},
		signup_members : async (obj, args, context, info) => {
			const collectionItemActor = context.children.get("users");
			if (obj.signup_members_ids) {
				obj.signup_members = await query(collectionItemActor, {search: {_id: {$in: obj.signup_members_ids}}}, 20000)
			}
			return obj.signup_members;
		}
	},
 
}