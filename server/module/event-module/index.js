import {GraphQLModule} from "@graphql-modules/core";

import resolvers from './resolvers';
import {readFileSync} from "fs";
import gql from "graphql-tag";
import MainModule from "../main-module";

export default function(ctx) {

    const schema = readFileSync(__dirname + '/schema.graphqls', 'utf8');


    const typeDefs = gql`
        ${schema}
    `;

    return new GraphQLModule({
        name: "EventModule",
        resolvers: resolvers,
        typeDefs: schema,
        imports: [MainModule(ctx)]

    });
}