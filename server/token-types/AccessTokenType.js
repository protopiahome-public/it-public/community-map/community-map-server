import TokenType from "./TokenType";

const uuidv1 = require('uuid/v1');

//https://github.com/square/js-jose



const url = "https://ecosystem.protopia.com";

// generate
// verify

/**
 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token as client_secret
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id
 * @param $aud The Audience, usually client URI
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $jti The "jwt token identifier", or nonce for this JWT
 *
 * @return string
 */

export default class AccessTokenType extends TokenType {

    async createToken(server, client, user = {}, scope = {}) {

        //claims
        // client_secret
        this.lifeTime = 60 * 60; // 1 hour.
        const issued_at = Date.now();
        const expires_at = issued_at+this.lifeTime;

        const sub = user ? user._id : client._id;

        this.json = {
            iss: server.url,
            sub: sub,
            aud: url,
            iat: issued_at,
            exp: expires_at,

            sid: uuidv1(),

            client_id: client._id,
            scp: scope
        };

        const string = await JSON.stringify(this.json);
        this.payload = await Buffer.from(string);

    }

    async createSignToken(server, client, user = {}, scope = {}){
        await this.createToken(server, client, user, scope);

        return await this.sign ();
    }

    async createCryptToken(server, client, user = {}, scope = {}){
        await this.createToken(server, client, user, scope);

        return await this.encrypt();
    }

}

//шифруется ключом авторизационного сервера

//https://medium.com/@darutk/oauth-access-token-implementation-30c2e8b90ff0