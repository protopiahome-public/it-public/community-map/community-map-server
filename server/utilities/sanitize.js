let sanitizeHtml = require('sanitize-html');

export default function (html) {
    return sanitizeHtml(html,
        {
            allowedTags: [ 'b', 'i', 'em', 'strong', 'a', 'p', 'div', 'ul', 'ol', 'li', 'blockquote', 'span' ],
            allowedAttributes: {
              'a': [ 'href' ],
              'p': [ 'class', 'style' ],
              'div': [ 'class', 'style' ],
            },
            allowedIframeHostnames: ['www.youtube.com']
          }
    );
}